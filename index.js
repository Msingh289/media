const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helmet = require("helmet");
const morgan = require("morgan");
const userRoute = require("./routes/users");
const authRoute = require("./routes/auth");
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
mongoose.connect("mongodb://localhost:27017/socialmediaDB", {useNewUrlParser:true, useUnifiedTopology: true},
function(err){
  if(err){
    console.log(err);
  }
  else{
    console.log("mongoDb connected");
  }}
);



//middleware
app.use(express.json());
app.use(helmet());
app.use(morgan("common"));
//middleware
app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);


// app.get("/", function(req,res){
//   console.log("WELCOME TO HOME PAGE");
// });
app.listen(3000, function(){
  console.log("server at port 3000");
});
