const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      require: true,
      min: 3,
      max: 20,
          },
    email: {
      type: String,
      required: true,
      max: 50,
        },
        password: {
     type: String,
     required: true,
     min: 6,
   },
    followers: {
      type: Array,
      default: [],
    },
    followings: {
      type: Array,
      default: [],
    },
    request: {
      type: Array,
      default: [],
    }
  }
);

module.exports = mongoose.model("User", UserSchema);
